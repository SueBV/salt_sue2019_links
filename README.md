# salt_SUE2019_links

Als je bij de saltstack presentatie bent geweest van 10 April dan ben je vast en zeker gekomen voor de linkjes die ik daar had genoemd!

# Salt demo code
https://gitlab.com/SnowBV/salt_sue2019_demo

# Training bij Snow
https://snow.nl/trainingen

# Repo saltstack (bootstrap)
https://repo.saltstack.com/

# Salt opzetten met Vagrant en Virtualbox
https://github.com/jorisdejosselin/testIACscripts

# Salt met pillars opzetten
https://github.com/jeroennijhof/saltpillar 
